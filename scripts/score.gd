extends Control

func _ready():
	$Menu_Panel/Label.text = str(Global.score)
	
	# hide next level button if score lower than minimum score
	if int(Global.score) < int(Global.minimum_score):
		$next_level.hide()
	
	# hide next level button after max level
	if Global.level_cleared == Global.max_level:
		# show win if min score passed or equal 
#		if int(Global.score) >= int(Global.minimum_score):
#			$Win.show()
		$next_level.hide()

func _on_back_pressed():
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_level_pressed():
	get_tree().change_scene("res://scenes/level_2.tscn")
	pass
