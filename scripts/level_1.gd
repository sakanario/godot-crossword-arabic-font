extends "res://scripts/level.gd"



func _init():
	level_name = "Level 1"
	level = 1
	game_duration = 180
	
	kunci_jawaban = {
		"1_mendatar" : "كلم",
		"5_mendatar" : "مذكر",
		"7_mendatar" : "تثنية",
		"4_mendatar" : "عامل",
		"8_mendatar" : "منادا",
		"9_mendatar" : "اظفة",
		
		"1_menurun" : "معرب",
		"3_menurun" : "مفرد",
		"6_menurun" : "لفظ",
		"10_menurun" : "نعت", 
	}
	question_list = {
		
		"1_mendatar" : {
			"title" : "1 Mendatar",
			"question" : "Kalimat sempurna disebut?"
		},
		"5_mendatar" : {
			"title" : "5 Mendatar",
			"question" : "Kata laki-kaki dalam istilah ilmu nahwu disebut juga?"
		},
		"4_mendatar" : {
			"title" : "4 Mendatar",
			"question" : "Lafadz yang bersandarkan pada perintah adalah?"
		},
		"7_mendatar" : {
			"title" : "7 Mendatar",
			"question" : "Sinonim dari angka 2 adalah?"
		},
		"8_mendatar" : {
			"title" : "8 Mendatar",
			"question" : "Kalimat panggilan disebut?"
		},
		"9_mendatar" : {
			"title" : "9 Mendatar",
			"question" : "Kalimat sandaran disebut?"
		},
		
		"1_menurun" : {
			"title" : "1 Menurun",
			"question" : "Lafadz yang bisa berubah disebut?"
		},
		"3_menurun" : {
			"title" : "3 Menurun",
			"question" : "Lafadz hanya menunjukan satu lafadz disebut?"
		},
		"6_menurun" : {
			"title" : "6 Menurun",
			"question" : "Kumpulan dari huruf disebut?"
		},
		"10_menurun" : {
			"title" : "10 Menurun",
			"question" : "Sinonim dari kata sifat adalah?"
		}
	}

func _on_back_pressed():
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/menu.tscn")


func _on_Keyboard_backspace():
	self._keyboard_backspace()


func _on_Timer_timeout():
	game_over()

func _on_WrongAnswer_timeout():
	resetQuestion()


