extends Control

var G 
# Called when the node enters the scene tree for the first time.
func _ready():
	self.G = get_node("/root/Global")
	audio_button_setup()
	music_button_setup()

func audio_button_setup():
	print("audio button")
	if !G.sound_fx:
		get_node("pengaturan_panel/Effect Sound/HBoxContainer/sound_on").modulate = "9a9a9a"
		get_node("pengaturan_panel/Effect Sound/HBoxContainer/sound_off").modulate = "ffffff"
	
	else :
		get_node("pengaturan_panel/Effect Sound/HBoxContainer/sound_on").modulate = "ffffff"		
		get_node("pengaturan_panel/Effect Sound/HBoxContainer/sound_off").modulate = "9a9a9a"

func music_button_setup():
	if !G.music:
		$pengaturan_panel/Music/HBoxContainer/music_on.modulate = "9a9a9a"
		$pengaturan_panel/Music/HBoxContainer/music_off.modulate = "ffffff"
	
	else :
		$pengaturan_panel/Music/HBoxContainer/music_on.modulate = "ffffff"		
		$pengaturan_panel/Music/HBoxContainer/music_off.modulate = "9a9a9a"

#func _on_sound_on_button_up():
#	G.sound_fx = true
#	audio_button_setup()
#
#
#func _on_sound_off_button_up():
#	G.sound_fx = false
#	audio_button_setup()


func _on_back_button_up():
	get_tree().change_scene("res://scenes/menu.tscn")


func _on_music_on_button_up():
	G.music = true
	music_button_setup()
	
func _on_music_off_button_up():
	G.music = false
	music_button_setup()


func _on_sound_on_button_up():
	G.sound_fx = true
	audio_button_setup()


func _on_sound_off_button_up():
	print("sound off")
	G.sound_fx = false
	audio_button_setup()
