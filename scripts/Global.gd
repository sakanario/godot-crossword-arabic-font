extends Node2D

var game_ver = 3.6

# config variable
var minimum_score = 60
var max_level = 2

# state variable
export var score = 0
var sound_fx = true
var music = true
var previous_level = null
var level_cleared = 0

func _ready():
	_load_game_state()
	$bgm.volume_db = -20
	if music :
		$bgm.play()

func _load_game_state():
	var save_game = File.new()
	if not save_game.file_exists("user://savefile.save"):
		print("save file not found")
		return # Error! We don't have a save to load.
	
	# Parsing game data
	save_game.open("user://savefile.save", File.READ)
	var data = parse_json(save_game.get_line())
	
	# Set level_cleared var
	self.level_cleared = data.level_cleared
	
	save_game.close()
	
	print("Game loaded")
	print(self.level_cleared_dict())

func save_game_state():
	var save_game = File.new()
	save_game.open("user://savefile.save", File.WRITE)
	save_game.store_line(to_json(self.level_cleared_dict()))
	save_game.close()
	
	print("Game saved")
	print(self.level_cleared_dict())

func level_cleared_dict():
	var data = {
		"level_cleared" : self.level_cleared
	}
	return data

func _process(delta):
	if music:
		# turn on
		if !$bgm.playing:
			$bgm.play()
	else:
		# turn off
		if $bgm.playing:
			$bgm.stop()


