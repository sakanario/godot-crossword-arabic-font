extends Control


func _ready():
	_disabled_locked_level()
	

func _disabled_locked_level():
	var button_container = $Menu_Panel/VBoxContainer
	for button in button_container.get_children():
		if  Global.level_cleared + 1 < int(button.name):
			button.disabled = true
			
func _on_back_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/menu.tscn")


func _on_1_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/level_1.tscn")


func _on_2_button_up():
	get_tree().change_scene("res://scenes/level_2.tscn")
	
