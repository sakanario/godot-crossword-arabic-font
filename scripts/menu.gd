extends Control

func _ready():
	$game_ver.text = "Game Version : " + str(Global.game_ver)

func _on_Mulai_pressed():
	get_tree().change_scene("res://scenes/level_1.tscn")

func _on_Level_pressed():
	get_tree().change_scene("res://scenes/level_select.tscn")

func _on_Pengaturan_button_up():
	get_tree().change_scene("res://scenes/pengaturan.tscn")

func _on_Info_button_up():
		get_tree().change_scene("res://scenes/info.tscn")

func _on_Exit_pressed():
	get_tree().quit()
