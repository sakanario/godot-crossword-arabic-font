extends Control

var bobot_soal = 10

var active_group = null # string
var active_node_index = 0
var max_index = 0
var nodes = []
var previous_group = null
var correct_answer = []
var previous_active_box = null
var current_active_box = null
var kunci_jawaban
var question_list
var level_name
var G 
var level
var game_duration
onready var isFreeze = false

func _ready():
	# reset score
	Global.score = 0
	print("G.score : " + str(Global.score))
	$ScorenTimer.setScore(Global.score)
	
	# declare global var
	G = get_node("/root/Global")
	$question_panel.set_level_text(level_name)
	
	# set game duration
	self._set_game_duration()

func _set_game_duration():
	$ScorenTimer/Timer.set_wait_time( self.game_duration )
	$ScorenTimer/Timer.start() 

# when keyboard is pressed
func on_keyboard_pressed(a_char):
	#check if active node index not null
	if active_node_index != null :
	# check if index not out of range
		if active_node_index <= nodes.size() - 1  :
			write_box(a_char)
			increment_index()
			skip_correct_box()
			manage_active_box()
			manage_answer()
			manage_highlight()
	
	print("index : " + str(active_node_index))

# when box is clicked 
func on_box_click(list_of_question_name): 
	# hide level info text
	$question_panel.hide_level()
	
	# make the question that has been answered correctly unselectable
	if !correct_answer.has(list_of_question_name[0]) && !isFreeze: 
		manage_active_question(list_of_question_name[0])
		manage_question_visibility()
		manage_indexing()
		manage_active_box()
		manage_highlight()
	print("box : " + str(current_active_box))

# ----- MANAGE FUNCTION START ----- 
func increment_index():
	# increment the index
	if active_node_index < nodes.size() - 1:
		active_node_index += 1

func skip_correct_box():
	# set index to first empty box
	if active_node_index < nodes.size() - 1:
		while nodes[active_node_index].correct:
			active_node_index += 1

func manage_active_box():
	# set previous box
	self.previous_active_box = self.current_active_box
	
	# set current active box
	if active_node_index < nodes.size():	
		self.current_active_box = nodes[active_node_index]

func manage_highlight():
	if previous_active_box != null :
		# unhighlight the previous box
		previous_active_box.unhighlight()
		
	if current_active_box != null:
		# highlight the active box
		current_active_box.highlight()
	

func manage_answer():
	# check answer only if the answer's char equal to correct's answer char
	if box_filled():
		# if the answer is right
		if check_answer(get_word(),active_group):
			
			# make the box's color turn darker
			get_tree().call_group(active_group, "mark_as_done")
			
			# append the question as correct answer
			correct_answer.append(active_group)
			
			# update global score var
			G.score = G.score + bobot_soal
			
			print("G.score : " + str(G.score))
			print("bobot soal : " + str(bobot_soal))
			
			# update score text
			$ScorenTimer.setScore(G.score)
			
			# move to score scene
			check_game_progress()
			
			#reset cursor
			active_node_index = null
			self.current_active_box = null
		else :
			freezeOnWrongAnswer()
			# the timer will trigger resetQuestion()
			$WrongAnswer.start()

func manage_indexing():
	# Mengarahkan pointer ke kotak pertama suatu grup kotak soal
	self.nodes = get_tree().get_nodes_in_group(self.active_group)
	
	# Sorting kotak, agar urutan inputnya benar
	self.sort_nodes()
	
	# set index
	self.active_node_index = 0
	if nodes[active_node_index].correct:
		active_node_index += 1
	
	# set max index
	self.max_index = self.nodes.size()

func manage_active_question(question):
	# set previous question
	if active_group != null:
		previous_group = active_group
		
		# unhiglight previous active group 
		get_tree().call_group(previous_group, "unhighlight_group")
		print("previous group : " + previous_group)
		
	# set current active question
	self.active_group = question
	print(self.active_group)
	
	# make the active group's box color turn lighter
	get_tree().call_group(active_group, "mark_as_selected")
	
	
	
func manage_question_visibility():
	$question_panel.set_question(question_list,active_group)
	

# ------ MANAGE FUNCTION END  ------ 


# ------ HELPER FUNCTION START  ------ 

func write_box(val):
	# replace the box's value with inputted char
	nodes[active_node_index].change_label(val)

func box_filled():
	var current_word = get_word()
	if current_word.length() == kunci_jawaban[active_group].length():
		return true
	return false

func get_word():
	var word = ""
	
	for box in nodes:
		word = word + box.get_char()
	return word

func sort_nodes():
#	print(self.nodes)
	for i in range(self.nodes.size()-1, -1, -1):
		for j in range(1,i+1,1):
			if self.nodes[j-1].name > self.nodes[j].name:
			  var temp = self.nodes[j-1]
			  self.nodes[j-1] = self.nodes[j]
			  self.nodes[j] = temp

func check_answer(answer,question_number):
	print("answer : " + answer)
	print("correct : " + self.kunci_jawaban[question_number])
	if answer == self.kunci_jawaban[question_number]:
		# play correct sound
		$SoundManager.playSound("Correct")
		return true
	else :
		# play wrong sound
		$SoundManager.playSound("Wrong")
		return false

func clear_box():
	for box in nodes:
		if !box.correct:
			box.change_label("")

func skip_correct_box_backwardly():
	if active_node_index > 0:
		active_node_index -= 1
		while nodes[active_node_index].correct:
			active_node_index -= 1

func _keyboard_backspace():
	# only delete uncorrect box
	skip_correct_box_backwardly()
	
	# set box's value to empty string 
	nodes[active_node_index].change_label("")
	
	manage_active_box()
	manage_highlight()

func resetQuestion():
	# hide cursor
	get_tree().call_group(active_group, "mark_as_selected")
	
	# clear uncorrect box
	clear_box()
		
	# reset the cursor to the first box
	active_node_index = 0
	skip_correct_box()
	self.current_active_box = nodes[active_node_index]
	
	manage_highlight()
	
	isFreeze = false

func freezeOnWrongAnswer():
	isFreeze = true
	get_tree().call_group(active_group, "unhighlight")
	get_tree().call_group(active_group, "mark_as_wrong")
# ------ HELPER FUNCTION END  ------ 

# ------ GAME PROGRESS FUNCTION START  ------ 

func check_game_progress():
	print("jumlah soal : " + str(kunci_jawaban.size()))
	print("jumlah jawaban benar : " + str(self.correct_answer.size()))
	
	# move to score scene when all question answered
	if correct_answer.size() >= kunci_jawaban.size():
		game_over()

func game_over():
	if int(Global.score) >= int(Global.minimum_score):
		update_cleared_level()
	go_to_score()

func go_to_score():
	G.previous_level = self.name
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/score.tscn")

func update_cleared_level():
	Global.level_cleared = self.level
	Global.save_game_state()

# ------ GAME PROGRESS FUNCTION END  ------ 
