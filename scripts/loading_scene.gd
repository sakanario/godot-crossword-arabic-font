extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$loading_bar/TextureProgress.value = 100 - float($Timer.time_left/$Timer.wait_time) * 100

func _on_Timer_timeout():
	get_tree().change_scene("res://scenes/menu.tscn")
