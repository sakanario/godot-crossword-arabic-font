extends "res://scripts/level.gd"

func _init():
	level_name = "Level 2"
	level = 2
	game_duration = 120
	
	kunci_jawaban = {
		"1_menurun" : "نكرة",
		"8_menurun" : "مبتدا",
		"6_menurun" : "اعرب",
		"7_menurun" : "فعل",
		"9_menurun" : "عطف",
		"3_menurun" : "جمع",
		
		"4_mendatar" : "معرفة",
		"2_mendatar" : "منعوت",
		"5_mendatar" : "مبني",
		"10_mendatar" : "لازم",
	}
	
	question_list = {
		# Menurun
		"1_menurun" : {
			"title" : "1 Menurun",
			"question" : "Kalimat umum disebut?"
		},
		"8_menurun" : {
			"title" : "8 Menurun",
			"question" : "Lafadz yang terdapat diawal kalimat disebut?"
		},
		"6_menurun" : {
			"title" : "6 Menurun",
			"question" : "Harakat akhir disebut?"
		},
		"7_menurun" : {
			"title" : "7 Menurun",
			"question" : "Kata kerja disebut?"
		},
		"9_menurun" : {
			"title" : "9 Menurun",
			"question" : "Kata sambung disebut?"
		},
		"3_menurun" : {
			"title" : "3 Menurun",
			"question" : "Lafadz yang menunjukan lebih dari orang dua orang disebut?"
		},
		
		# Mendatar
		"4_mendatar" : {
			"title" : "4 Mendatar",
			"question" : "Kalimat khusus disebut?"
		},
		"2_mendatar" : {
			"title" : "2 Mendatar",
			"question" : "Lafadz yang disipati?"
		},
		"5_mendatar" : {
			"title" : "5 Mendatar",
			"question" : "Kata yang tidak bisa berubah?"
		},
		"10_mendatar" : {
			"title" : "10 Mendatar",
			"question" : "Kata kerja yang tidak butuh objek disebut?"
		},
	}
	
func _ready():
#	print(question["1_mendatar"]["title"])
	pass
	
func _on_back_pressed():
	# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/menu.tscn")


func _on_Keyboard_backspace():
	self._keyboard_backspace()


func _on_Timer_timeout():
	game_over()

func _on_WrongAnswer_timeout():
	resetQuestion()


func _on_clear_level_button_up():
	Global.score = 50
	game_over()
