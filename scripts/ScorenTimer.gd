extends TextureRect

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	# Set timer
#	$Timer.set_wait_time( Global.level_2_duration ) 
#	$Timer.start()
	
func setScore(val):
	$Score.text = str(val)

# warning-ignore:unused_argument
func _process(delta):
	$TimerLabel.text = generate_time()

func generate_time():
	var minute = $Timer.time_left/60
	var second = fmod($Timer.time_left,60)
	
#	print(minute + " : " + second)
	
	return str(floor(minute)) + ":" + str(round(second))
