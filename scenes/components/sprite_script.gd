extends Sprite

func change_label_val():
	print("change label")
	$Label.text = "ب"

func _input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		if get_rect().has_point(to_local(event.position)):
			self._set_active_group()
			
func _set_active_group():
	var group_name = get_parent().get_groups()
	get_parent().get_parent().get_parent().on_box_click(group_name)

