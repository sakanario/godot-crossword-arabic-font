extends Control

onready var correct = false

# unselected group = darker
# selected group = lighter

func _ready():
	# turn box into kinda dark
	$sprite.modulate = "b0b0b0"
	pass

func change_label(val):
	$Label.text = str(val)
	
func get_char():
	return $Label.text

# ----- Box Color Start ----- 
func highlight():
	if !self.correct:
		$Cursor.show()

func unhighlight():
	if !self.correct:
		$Cursor.hide()
# ----- Box Color End ----- 

# ----- Group Color Start ----- 
func mark_as_selected():
	if !self.correct:
		$sprite.modulate = "ffffff"
		
func unhighlight_group():
	if !self.correct:
		$sprite.modulate = "b0b0b0"

func mark_as_done():
	self.correct = true
	$sprite.modulate = "9dff66"

func mark_as_wrong():
	if !self.correct:
		$sprite.modulate = "fb3838"
# ----- Group Color End ----- 
