extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func set_question(questions,question_name):
#	$panel/title.text = questions[question_name]["title"]
	$panel/question.text = questions[question_name]["question"]

func set_level_text(level):
	$panel/level.text = level

func hide_level():
	if $panel/level.visible:
		$panel/level.hide()
