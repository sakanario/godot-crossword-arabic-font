extends Control

var G
# Called when the node enters the scene tree for the first time.
func _ready():
	self.G = get_node("/root/Global")

func playSound(sound_name):
	if G.sound_fx:
		get_node(sound_name).play()
