extends Control

var G
signal backspace

# Called when the node enters the scene tree for the first time.
func _ready():
	G = get_node("/root/Global")
	$keyboard_sound.volume_db = -10

func change_parent_attr_val(val):
	var parent = get_parent()
	parent.on_keyboard_pressed(val)
	play_keyboard_sound()

func play_keyboard_sound():
	if G.sound_fx:
		$keyboard_sound.play()
		
func _on_dho_pressed():
	self.change_parent_attr_val("ض")

func _on_so_pressed():
	self.change_parent_attr_val("ص")

func _on_tsa_pressed():
	self.change_parent_attr_val("ث")

func _on_qaf_pressed():
	self.change_parent_attr_val("ق")

func _on_fa_pressed():
	self.change_parent_attr_val("ف")

func _on_gho_pressed():
	self.change_parent_attr_val("غ")

func _on_ain_pressed():
	self.change_parent_attr_val("ع")
	
func _on_ha_pressed():
	self.change_parent_attr_val("ه")
	
func _on_khoh_pressed():
	self.change_parent_attr_val("خ")

func _on_hah_pressed():
	self.change_parent_attr_val("ح")


func _on_jeem_pressed():
	self.change_parent_attr_val("ج")


# ----------- baris ke dua -----------

func _on_sya_pressed():
	self.change_parent_attr_val("ش")
	
func _on_sa_pressed():
	self.change_parent_attr_val("س")

func _on_ya1_pressed():
	self.change_parent_attr_val("ي")
	
func _on_ba_pressed():
	self.change_parent_attr_val("ب")

func _on_lam_pressed():
	self.change_parent_attr_val("ل")

func _on_alif_pressed():
	self.change_parent_attr_val("ا")

func _on_ta_pressed():
	self.change_parent_attr_val("ت")

func _on_na_pressed():
	self.change_parent_attr_val("ن")


func _on_meem_pressed():
	self.change_parent_attr_val("م")

func _on_ka_pressed():
	self.change_parent_attr_val("ك")

func _on_ta2_pressed():
	self.change_parent_attr_val("ة")

# ----------- baris ke ketiga -----------

func _on_ain2_pressed():
	self.change_parent_attr_val("ء")

func _on_zah_pressed():
	self.change_parent_attr_val("ظ")

func _on_tho_pressed():
	self.change_parent_attr_val("ط")
	
func _on_dzal_pressed():
	self.change_parent_attr_val("ذ")
	
func _on_dza_pressed():
	self.change_parent_attr_val("د")
	
func _on_za_pressed():
	self.change_parent_attr_val("ز")
	
func _on_ra_pressed():
	self.change_parent_attr_val("ر")

func _on_wau_pressed():
	self.change_parent_attr_val("و")
	
func _on_ya_pressed():
	self.change_parent_attr_val("ى")
	
func _on_backspace_button_up():
	emit_signal("backspace")
